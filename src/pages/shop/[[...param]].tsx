import { useRouter } from "next/router";

const ShopDetail = () => {
  const { query } = useRouter();

  console.log(query);
  return (
    <>
      <p>ShopDetail</p>
      <p>Name: {query.param ? query.param[0] : ""} </p>
    </>
  );
};

export default ShopDetail;
