import React, { useState } from "react";
import { Inter } from "next/font/google";
import "bootstrap/dist/css/bootstrap.min.css";
import { Button, Form, Modal, Stack, Table, Toast } from "react-bootstrap";
import { useFetchUsers } from "@/features/users/useFetchUsers";
import { useQueryClient } from "@tanstack/react-query";
import { useAddUser } from "@/features/users/useAddUser";
import { useDeleteUserById } from "@/features/users/useDeleteUsers";
import AddModal from "@/component/addModal";

const inter = Inter({ subsets: ["latin"] });

export interface User {
  id: number;
  name: string;
  username: string;
  email: string;
}

export default function Home() {
  const queryClient = useQueryClient();
  // const [initialValues, setInitialValues] = useState<User>({
  //   id: 0,
  //   name: "",
  //   username: "",
  //   email: "",
  // });
  const [id, setId] = useState(0);
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [username, setUsername] = useState("");

  const clear = () => {
    setEmail("");
    setName("");
    setUsername("");
  };

  const [showAddModal, setShowAddModal] = useState(false);

  const handleShowAddModal = () => setShowAddModal(true);
  const handleCloseAddModal = () => setShowAddModal(false);

  // const clear = () => {
  //   setInitialValues({
  //     id: 0,
  //     name: "",
  //     username: "",
  //     email: "",
  //   });
  // };

  const {
    data: dataUsers,
    isLoading: isLoadingUser,
    isError: isErrorUser,
    refetch,
  } = useFetchUsers();
  console.log(dataUsers, "data");

//add user
  const { mutate: addUser } = useAddUser({
    onSuccess: () => {
      handleCloseAddModal();
      refetch();
      // queryClient.invalidateQueries({ queryKey: ["fetch-users"] });
      clear();
    },
    onError: (error: any) => {
      return (
        <Toast>
          <Toast.Body>{error.message}</Toast.Body>
        </Toast>
      );
    },
  });

  const { mutate: deleteUser } = useDeleteUserById({
    onSuccess: () => {
      refetch();
      console.log("User deleted successfully");
    },
  });

  // const addHandler = () => {
  //   addUser(initialValues);
  // };

  const addHandler = () => {
    addUser({
      id: id,
      name: name,
      email: email,
      username: username,
    });
  };

  const deleteHandler = (userId: number) => {
    deleteUser(userId);
  };

  const renderUsers = () => {
    return dataUsers?.map((user: User) => {
      return (
        <>
          <tr key={user.id}>
            <td>{user.id}</td>
            <td>{user.name}</td>
            <td>{user.username}</td>
            <td>{user.email}</td>
            <td>
              <Button variant="outline-dark">Edit</Button>
            </td>
            <td>
              <Button
                variant="outline-danger"
                onClick={() => deleteHandler(user.id)}
              >
                Delete {user.id}
              </Button>
            </td>
          </tr>
        </>
      );
    });
  };

  // const onHandleInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
  //   const { name, value } = event.target;

  //   setInitialValues((prev) => ({
  //     ...prev,
  //     [name]: value,
  //   }));
  // };

  if (isLoadingUser)
    return (
      <Toast>
        <Toast.Body>Loading Users Data</Toast.Body>
      </Toast>
    );
  if (isErrorUser)
    return (
      <Toast>
        <Toast.Body>There was an Error</Toast.Body>
      </Toast>
    );

  return (
    <>
      <Stack direction="horizontal" gap={5}>
        <div className="p-2">Data Users</div>
        <div className="p-2 ms-auto">
          <Button onClick={handleShowAddModal}>Add User</Button>
        </div>
      </Stack>
      <Table striped bordered hover className="text-center">
        <thead>
          <tr>
            <th>No</th>
            <th>Name</th>
            <th>Username</th>
            <th>Email</th>
            <th colSpan={2}>Action</th>
          </tr>
        </thead>
        <tbody>{renderUsers()}</tbody>
      </Table>
      <AddModal
        showModal={showAddModal}
        handleCloseModal={handleCloseAddModal}
        handleChange={(e) => setName(e.target.value)}
      />

      <Modal show={showAddModal} onHide={handleCloseAddModal}>
        <Modal.Header closeButton>
          <Modal.Title>Add User</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form>
            <Form.Group className="mb-3">
              <Form.Label>Name</Form.Label>
              <Form.Control
                type="text"
                placeholder="Name"
                name="name"
                value={name}
                onChange={(e) => setName(e.target.value)}
              />
            </Form.Group>
            <Form.Group className="mb-3">
              <Form.Label>Username</Form.Label>
              <Form.Control
                type="text"
                placeholder="Username"
                name="username"
                value={username}
                onChange={(e) => setUsername(e.target.value)}
              />
            </Form.Group>
            <Form.Group className="mb-3">
              <Form.Label>Email</Form.Label>
              <Form.Control
                type="email"
                placeholder="Email"
                name="email"
                value={email}
                onChange={(e) => setEmail(e.target.value)}
              />
            </Form.Group>
            <Modal.Footer>
              <Button variant="primary" onClick={addHandler}>
                Save
              </Button>
            </Modal.Footer>
          </Form>
        </Modal.Body>
      </Modal>
    </>
  );
}
