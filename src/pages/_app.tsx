import "@/styles/globals.css";
import type { AppProps } from "next/app";
import AppShell from "./layouts/AppShell";
import { QueryClient, QueryClientProvider } from "@tanstack/react-query";

const queryClient = new QueryClient({
  defaultOptions: {
    queries: {
      refetchOnWindowFocus: false,
    },
  },
});

export default function App({ Component, pageProps }: AppProps) {
  return (
    <>
      <QueryClientProvider client={queryClient}>
        <AppShell>
          <Component {...pageProps} />
        </AppShell>
      </QueryClientProvider>
    </>
  );
}
