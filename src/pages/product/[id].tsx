import { useRouter } from "next/router";

const DetailProduct = () => {
  //   const router = useRouter();
  //   console.log(router);

  const { query } = useRouter();
  return (
    <>
      <p>Detail Product</p>
      <p>Product Id: {query.id}</p>
    </>
  );
};

export default DetailProduct;
