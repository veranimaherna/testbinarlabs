import { useRouter } from "next/router";
import { useEffect, useState } from "react";

const Product = () => {
  const [isLogin, setIsLogin] = useState(false);
  const router = useRouter();
  useEffect(() => {
    if (!isLogin) {
      router.push("/auth/login");
    }
  }, []);
  return (
    <>
      <div>Product</div>
    </>
  );
};

export default Product;
