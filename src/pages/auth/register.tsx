import Link from "next/link";
import { useRouter } from "next/router";

const RegisterPage = () => {
  const { push } = useRouter();

  const handleRegister = () => {
    push("/auth/login");
  };
  return (
    <>
      <h1>Register Page</h1>
      <button onClick={() => handleRegister()}>REGISTER</button>
      <p>
        Sudah punya akun? <Link href={"/auth/login"}>Klik disini</Link>
      </p>
    </>
  );
};

export default RegisterPage;
