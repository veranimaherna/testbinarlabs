import Link from "next/link";
import { useRouter } from "next/router";

const LoginPage = () => {
  const router = useRouter();

  const handleLogin = () => {
    router.push("/product");
  };
  return (
    <div>
      <h1>LoginPage</h1>
      <button onClick={() => handleLogin()}>LOGIN</button>
      <p>
        Belum punya akun? <Link href={"/auth/register"}>Klik Disini</Link>
      </p>
    </div>
  );
};

export default LoginPage;
