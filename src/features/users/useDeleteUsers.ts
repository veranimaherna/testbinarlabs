import { useMutation } from "@tanstack/react-query"
import { axiosInstance } from "../../lib/axios";

export const useDeleteUserById = ({ onSuccess, onError }: any) => {
    return useMutation({
        mutationFn: async (id: number) => {
            const dataResponse = await axiosInstance.delete(`/users/${id}`)

            return dataResponse
        },
        onSuccess,
        onError
    })
}