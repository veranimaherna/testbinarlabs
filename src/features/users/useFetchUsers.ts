import { User } from "@/pages";
import { axiosInstance } from "../../lib/axios";
import { useQuery } from "@tanstack/react-query";

export const useFetchUsers = () => {
    return useQuery({
        queryFn: async () => {
            const dataResponse = await axiosInstance.get("/users")

            return dataResponse.data as User[]
        },
        queryKey: ["fetch-users"]
    })
}