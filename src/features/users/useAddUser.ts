import { axiosInstance } from "@/lib/axios"
import { User } from "@/pages"
import { useMutation } from "@tanstack/react-query"

export const useAddUser = ({ onSuccess }: any) => {
    return useMutation({
        mutationFn: async (data: User) => {
            const dataResponse = await axiosInstance.post("/users", data)

            return dataResponse
        },
        onSuccess
    })
}