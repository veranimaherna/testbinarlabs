import { axiosInstance } from "@/lib/axios"
import { user } from "@/pages"
import { useMutation } from "@tanstack/react-query"

export const useEditUser = ({ onSuccess }: any) => {
    return useMutation({
        mutationFn: async (data: user) => {
            const dataResponse = await axiosInstance.patch(`/users/${data.id}`, data)

            return dataResponse;
        },
        onSuccess,
    })
}