// import { user } from "@/pages";
// import { useState } from "react";
// import { Button, Form, Modal } from "react-bootstrap";

// const editUserModal = ({ name, username, email }: user) => {
//   const [showEditModal, setShowEditModal] = useState(false);

//   const handleShowEditModal = () => setShowEditModal(true);
//   const handleCloseEditModal = () => setShowEditModal(false);
//   return (
//     <>
//       <Modal show={showEditModal} onHide={handleCloseEditModal}>
//         <Modal.Header closeButton>
//           <Modal.Title>Edit User</Modal.Title>
//         </Modal.Header>
//         <Modal.Body>
//           <Form>
//             <Form.Group className="mb-3">
//               <Form.Label>Name</Form.Label>
//               <Form.Control
//                 type="text"
//                 placeholder="Name"
//                 value={name}
//                 onChange={(e) => setName(e.target.value)}
//               />
//             </Form.Group>
//             <Form.Group className="mb-3">
//               <Form.Label>Username</Form.Label>
//               <Form.Control
//                 type="text"
//                 placeholder="Username"
//                 value={username}
//                 onChange={(e) => setUsername(e.target.value)}
//               />
//             </Form.Group>
//             <Form.Group className="mb-3">
//               <Form.Label>Email</Form.Label>
//               <Form.Control
//                 type="email"
//                 placeholder="Email"
//                 value={email}
//                 onChange={(e) => setEmail(e.target.value)}
//               />
//             </Form.Group>
//             <Modal.Footer>
//               <Button variant="primary" onClick={editHandler}>
//                 Save
//               </Button>
//             </Modal.Footer>
//           </Form>
//         </Modal.Body>
//       </Modal>
//     </>
//   );
// };
