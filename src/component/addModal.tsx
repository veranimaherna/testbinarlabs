import { User } from "@/pages";
import { ChangeEventHandler } from "react";
import { Button, Form, Modal } from "react-bootstrap";

type AddModalProps = {
    showModal: boolean;
    handleCloseModal: () => void;
    handleChange: ChangeEventHandler<HTMLInputElement>;
  };

  const AddModal: React.FC<AddModalProps> = ({ showModal, handleCloseModal, handleChange }) => {
  return (
    <Modal show={true} onHide={() => handleCloseModal}>
      <Modal.Header closeButton>
        <Modal.Title>Add User</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Form>
          <Form.Group className="mb-3">
            <Form.Label>Name</Form.Label>
            <Form.Control
              type="text"
              placeholder="Name"
              name="name"
              onChange={handleChange}
            />
          </Form.Group>
          <Form.Group className="mb-3">
            <Form.Label>Username</Form.Label>
            <Form.Control
              type="text"
              placeholder="Username"
              name="username"
              onChange={handleChange}
            />
          </Form.Group>
          <Form.Group className="mb-3">
            <Form.Label>Email</Form.Label>
            <Form.Control
              type="email"
              placeholder="Email"
              name="email"
              onChange={handleChange}
            />
          </Form.Group>
          <Modal.Footer>
            <Button variant="primary">
              Save
            </Button>
          </Modal.Footer>
        </Form>
      </Modal.Body>
    </Modal>
  );
};

export default AddModal;
